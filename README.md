# Sink Boilerplate

This repo is a boilerplate for `sink`, the component that deals with events published into the broker.

## Make it your own

To make your own repo, fork this boilerplate and you're good to go!

## Docker

```sh
# builds the image
docker-compose build

# runs for development
docker-compose up

# clear all containers with its volumes
docker-compose down -v

# run tests
docker-compose run sink npm run test
```
